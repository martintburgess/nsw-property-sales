source("setup.R")

full_build <- FALSE # this can take a while so default is not to run it all

if (full_build) {
  
  source("processing/01_download_unzip.R")
  source("processing/02_b_records_to_csv.R")
  source("processing/04_compile_all_years.R")
  
} else {
  
  source("processing/03_update_this_year.R")
  
}

