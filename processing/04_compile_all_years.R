df <- tibble()

for(the_file in list.files("processed-data/b_records", full.names = TRUE)){
  year_data <- read_csv(the_file,
                        col_types = cols(
                          .default = col_character(),
                          property_id = col_double(),
                          sale_counter = col_double(),
                          download_datetime = col_datetime(format = ""),
                          unit_number = col_character(),
                          postcode = col_character(),
                          area = col_double(),
                          contract_date = col_datetime(format = ""),
                          settlement_date = col_datetime(format = ""),
                          purchase_price = col_double(),
                          strata_lot_number = col_double(),
                          percent_sold = col_double()
                        ))
  
  df <- bind_rows(df,
                  year_data)
}

write_csv(df, 
          "processed-data/property_sales_2001_2020.csv")

zip("processed-data/property_sales_2001_2020.zip", 
    c("processed-data/property_sales_2001_2020.csv"))